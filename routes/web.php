<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// define global variable
view()->share('version', time());
view()->share('host_name', 'AA2888 Cambodia');
view()->share('meta_description', 'យើងផ្តល់ជូនសេវាកម្មកម្សាន្តគ្មានដែនកំណត់និងរហ័សទាន់ចិត្តងាយស្រួលដក/ដាក់ប្រាក់24ម៉ោង មានទំនុកចិត្តជាងគេនៅកម្ពុជា ព្រមទាំងទទួលបានពត៌មានកីឡាជាតិ និង អន្តរជាតិ យ៉ាងសម្បូរបែប។');
view()->share('img_root', '/');
view()->share('default_social_image', asset('img/logo-512.jpg'));

// Views
Route::get('/', 'HomeController@index')->name('home');
Route::get('/sport', 'SportController@index')->name('sport');
Route::get('/casino', 'CasinoController@index')->name('casino');
Route::get('/live-casino', 'LiveCasinoController@index')->name('live-casino');
Route::get('/live-football', 'LiveFootballController@index')->name('live-football');
Route::get('/live-football/{id}', 'LiveFootballController@show')->name('live-football.show');

// Auth
Auth::routes();

// Admin group
Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
    Route::get('/', function() {
        return redirect('/admin/dashboard');
    });
    Route::get('dashboard', 'admin\DashboardController@index')->name('dashboard');

    // User
    Route::get('user', 'admin\UserController@index')->name('user');
});
