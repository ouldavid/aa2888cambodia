jQuery(document).ready(function($) {
    // active menu
    var url = window.location;
    $('.navbar-nav a[href="'+ url +'"]').parent().addClass('active');
    $('.navbar-nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');
    // active dropdown menu
    $('.navbar-nav .dropdown a[href="'+ url +'"]').parents('li.dropdown').addClass('active');
    $('.navbar-nav .dropdown a').filter(function() {
        return this.href == url;
    }).parents('li.dropdown').addClass('active');

    //window and animation items
    var animation_elements = $.find('.animation-element');
    var web_window = $(window);
    //check to see if any animation containers are currently in view
    function check_if_in_view() {
        //get current window information
        var window_height = web_window.height();
        var window_top_position = web_window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);

        //iterate through elements to see if its in view
        $.each(animation_elements, function() {
            //get the element sinformation
            var element = $(this);
            var element_height = $(element).outerHeight();
            var element_top_position = $(element).offset().top;
            var element_bottom_position = (element_top_position + element_height);

            if(element.hasClass('divLine')) { return true; } // if already animated skip to the next item

            //check to see if this current container is visible (its viewable if it exists between the viewable space of the viewport)
            if((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
                element.addClass('divLine');
            }else {
                element.removeClass('divLine');
            }
        });
    }
    //on or scroll, detect elements in view
    $(window).on('scroll resize', function() {
        check_if_in_view()
    });
    //trigger our scroll event on initial load
    $(window).trigger('scroll');

    // search overlay
    $("button#search-btn").click(function() {
        $(".search-overlay").fadeIn();
    });
    $(".search-overlay button#close-btn").click(function() {
        $(".search-overlay").fadeOut();
        $(".search-overlay").close();
    });

    // button close, advertising
    $("#ads-header button.close").click(function() {
        $(this).parent().hide();
    });

    // b-lazy image
    var bLazy = new Blazy({
        breakpoints: [
            {
                width: 575.98, // max-width
                src  : 'data-src-xs'
            }, {
                width: 767.98, // max-width
                src  : 'data-src-sm'
            }, {
                width: 991.98, // max-width
                src  : 'data-src-md'
            }
        ],
        success: function(element) {
            setTimeout(function() {
                // We want to remove the loader gif now.
                // First we find the parent container
                // then we remove the "loading" class which holds the loader image
                var parent = element.parentNode;
                parent.className = parent.className.replace(/\bloading\b/,'');
            }, 200);
        }
    });

});

// header position fixed
window.onscroll = function() {navbarSticky()};
var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;
function navbarSticky() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}

// Navigation Header
window.onload=function() {
    if(window.jQuery) {
        $(document).ready(function() {
            $(".sidebarNavigation .navbar-collapse").hide().clone().appendTo("body").removeAttr("class").addClass("sideMenu").show();
            $("body").append("<div class='overlay'></div>");
            $(".navbar-toggle, .navbar-toggler").on("click",function() {
                $(".sideMenu").addClass($(".sidebarNavigation").attr("data-sidebarClass"));
                $(".sideMenu, .overlay").toggleClass("open");
                $(".overlay").on("click",function(){$(this).removeClass("open");$(".sideMenu").removeClass("open")})
            });
            $("body").on("click",".sideMenu.open .close",function() {
                $(".sideMenu, .overlay").toggleClass("open");
            });
           /* $("body").on("click",".sideMenu.open .nav-item",function() {
                if(!$(this).hasClass("dropdown")){$(".sideMenu, .overlay").toggleClass("open")}
            });*/
            $(window).resize(function() {
                if($(".navbar-toggler").is(":hidden")){$(".sideMenu, .overlay").hide()}else{$(".sideMenu, .overlay").show()}
            })
        })
    }else{console.log("sidebarNavigation Requires jQuery")}
}

// Navigation search
/*(function($) {
    // Handle click on toggle search button
    $('#toggle-search').click(function() {
        $('#search-form, #toggle-search').toggleClass('open');
        return false;
    });

    // Handle click on search submit button
    $('#search-form input[type=submit]').click(function() {
        $('#search-form, #toggle-search').toggleClass('open');
        return true;
    });

    // Clicking outside the search form closes it
    $(document).click(function(event) {
        var target = $(event.target);

        if (!target.is('#toggle-search') && !target.closest('#search-form').size()) {
            $('#search-form, #toggle-search').removeClass('open');
        }
    });

})(jQuery);*/

// reload table
function reload_page() {
	window.location.href=window.location.href;
}
