$(document).ready(function() {
    // banner slide
    $('#banner-slide').slick({
        slidesToShow: 1,
        infinite: true,
        autoplay: false,
        dots: true,
        arrows: false,
        /*nextArrow: '<img class="right" src="assets/img/banner/arrow-right.png" />',
        prevArrow: '<img class="left" src="assets/img/banner/arrow-left.png" />',*/
        speed: 500,
        autoplaySpeed: 5000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear'
    });
    // football slide
    $('#football-slide').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: true,
        nextArrow: '<button class="right d-flex align-items-center"><img src="img/arrow/arrow.png" alt="arrow right"></button>',
        prevArrow: '<button class="left d-flex align-items-center"><img src="img/arrow/arrow.png" alt="arrow left"></button>',
        speed: 500,
        autoplaySpeed: 7000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear',
    });

});
