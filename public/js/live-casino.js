$(document).ready(function() {
    // banner slide
    $('#banner-slide').slick({
        slidesToShow: 1,
        infinite: true,
        autoplay: false,
        dots: true,
        arrows: false,
        /*nextArrow: '<img class="right" src="assets/img/banner/arrow-right.png" />',
        prevArrow: '<img class="left" src="assets/img/banner/arrow-left.png" />',*/
        speed: 500,
        autoplaySpeed: 5000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear'
    });

});
