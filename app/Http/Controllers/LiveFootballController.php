<?php

namespace App\Http\Controllers;

use http\Env;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LiveFootballController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $data= Http::get(env('API_URL','api.aa2888cambodia.com').'/api/front/competitions/matches');
        $items= $data['data']['items'];
        return view('live-football', compact('items'));
    }
    public function show($id)
    {
        $data= Http::get(env('API_URL','api.aa2888cambodia.com').'/api/front/competitions/matches');
        $items= $data['data']['items'];
        $item= Http::get(env('API_URL','api.aa2888cambodia.com').'/api/front/matches/'.$id);
        return view('live-football-show',compact(['item','items']));
    }
}
