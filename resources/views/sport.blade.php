@extends('layouts.master')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())).' | '.$host_name)
@section('title-social', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

<!-- include banner -->
@section('banner')
    <div id="banner-slide">
        <div class="slide">
            <div class="bg-slide" style="background-image: url({{ asset('img/banner/slide-02.jpg?v='.$version) }})"></div>
            <img class="img-fluid img-slide" src="{{ asset('img/banner/slide-02-mobile.jpg?v='.$version) }}" alt="{{ $host_name }}" />
        </div>
    </div>
@endsection

@section('content')
    <div class="position-relative">
        <div id="particles-js"></div>

        <div class="container py-5">
            <div id="sportCasino" class="position-relative mb-3">
                <div class="d-flex justify-content-center mb-3">
                    <h5 class="color-gold text-uppercase">Sports</h5>
                </div>
                <div class="row">
                    <div class="col-md-12 col-12 mb-3 d-flex justify-content-center">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/sports/sport-01.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                </div>
            </div><!-- #sportCasino -->
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
