<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <meta name="robots" content="index, follow">
    <meta name="description" content="@yield('description')" />
    {{--<meta name="keywords" content="Online Casino, Sports Betting, Gambling, Football, Asian Player" />--}}
    <meta name="author" content="{{ $host_name }}" />

    <!-- google verify -->

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@" />
    <meta name="twitter:creator" content="@" />
    <meta name="twitter:domain" content="{{ Request::root() }}" />
    <meta name="twitter:url" content="{{ url()->current() }}" />
    <meta name="twitter:title" content="@yield('social-title')" />
    <meta name="twitter:description" content="@yield('description')" />
    <meta name="twitter:image" content="@yield('social-image')" />

    <!-- FB meta -->
    <meta property="fb:pages" content="" />
    <meta property="fb:app_id" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:title" content="@yield('social-title')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@yield('social-image')" />

    <!-- Canonical -->
    <link rel="canonical" href="{{ url()->current() }}" />

    <!-- app -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- bootstrap icons -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-icons-1.5.0/font/bootstrap-icons.css') }}">
    <!-- master style -->
    <link href="{{ asset('css/common.css?v='.$version) }}" rel="stylesheet" type="text/css" />
    <!-- page style -->
    @yield('css')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-M5QSTGXVJT"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-M5QSTGXVJT');
    </script>

    <!-- share social button -->
    {{--<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f5e0d4b5503590012dc87f0&product=sop' async='async'></script>--}}
</head>
<body>
        <header>
            @include('templates.header')
        </header>

        <!-- banner slide -->
        @yield('banner')

        <!-- content -->
        @yield('content')

        <footer>
            @include('templates.footer')
        </footer>

        <!-- Import JS -->
        <script src="{{ asset('js/app.js') }}" ></script>
        <script src="{{ asset('js/blazy.min.js') }}" ></script>
        <!-- bg particles -->
        <script src="{{ asset('plugins/particles/js/particles.js') }}" ></script>
        <script src="{{ asset('plugins/particles/js/app.js') }}" ></script>
        <!-- master js -->
        <script type="text/javascript" src="{{ asset('js/common.js?v='.$version) }}"></script>
        <!-- script page -->
        @yield('script')

        <!-- Facebook -->
        <!-- Messenger Chat Plugin Code -->
        <div id="fb-root"></div>

        <!-- Your Chat Plugin code -->
        <div id="fb-customer-chat" class="fb-customerchat">
        </div>

        <script>
            var chatbox = document.getElementById('fb-customer-chat');
            chatbox.setAttribute("page_id", "614516965282208");
            chatbox.setAttribute("attribution", "biz_inbox");
            window.fbAsyncInit = function() {
                FB.init({
                    xfbml            : true,
                    version          : 'v11.0'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        {{--
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId            : '747873512712202',
                    autoLogAppEvents : true,
                    xfbml            : true,
                    version          : 'v7.0'
                });
            };
        </script>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>--}}
</body>
</html>
