@extends('layouts.master')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())).' | '.$host_name)
@section('title-social', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
   {{-- <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">--}}
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

<!-- include banner -->
@section('banner')
   {{-- <div id="banner-slide">
        <div class="slide">
            <div class="bg-slide" style="background-image: url({{ asset('img/banner/slide-01.jpg?v='.$version) }})"></div>
            <img class="img-fluid img-slide" src="{{ asset('img/banner/slide-01-mobile.jpg?v='.$version) }}" alt="{{ $host_name }}" />
        </div>
    </div>--}}
@endsection

@section('content')
    <div class="position-relative">
        <div id="particles-js"></div>

        <div class="container-fluid container-lg px-0 px-sm-3 py-5">
            <!-- ads banner -->
            <div class="row">
                <div class="col-12 mb-3">
                    <a href="https://www.messenger.com/t/614516965282208" target="_blank"><img class="img-fluid" src="{{ asset("img/ads/aa2888-euro.png") }}" alt="AA2888 Cambodia"></a>
                </div>
            </div>

            <div class="row">
                @foreach (@$items as $item)
                <div class="col-12">
                    <div class="d-flex justify-content-center bg-calendar p-3">
                        <img class="img-fluid" src="{{ asset(@$item['logo']) }}" alt="Premier League">
                        <h5 class="d-flex align-items-center text-white mb-0 px-3">{{@$item['name']}}</h5>
                    </div>

                    @foreach(@$item['matches'] as $match)
                        <div class="bg-match p-3">
                            <div class="row">
                                <div class="col-10 col-md-3 order-1 mb-2 mb-md-0">
                                    <div class="d-flex justify-content-start justify-content-md-center text-white">
                                        <div class="float-left mr-2 mr-md-3">
                                            <i class="far fa-calendar-alt"></i>
                                        </div>
                                        <div class="float-left text-center calendar-play">
                                            <p class="d-inline d-md-block font-14 mb-0">{{@$match['match_date']}}</p>
                                            <p class="d-inline d-md-block font-14 mb-0">({{@$match['match_time']}})</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 order-2 px-0">
                                    <table cellpadding="0" cellspacing="0" border="0" class="w-100">
                                        <tr>
                                            <td>
                                                <div class="football-club">
                                                    <span class="font-14 font-weight-bold">{{@$match['home_name']}}</span>
                                                    <img class="img-fluid ml-1 ml-md-2" src="{{ asset(@$match['home_logo']) }}" alt="{{@$match['home_name']}}">
                                                </div>
                                            </td>
                                            <td>
                                                @if(\Carbon\Carbon::now()->between(
                                                    @$match['match_date_time'],
                                                    \Carbon\Carbon::parse(@$match['match_date_time'])->addMinutes(95)->format('Y-m-d H:i:s'), true))
                                                    <span><img class="img-fluid m-auto w-50 h-auto" src="{{ asset("img/footballs/live.gif") }}" alt="AA2888 Cambodia" /></span>
                                                @else
                                                    <strong class="font-14 text-stroke-white px-1">VS</strong>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="football-club">
                                                    <img class="img-fluid mr-1 mr-md-2" src="{{ asset(@$match['away_logo']) }}" alt="{{@$match['away_name']}}">
                                                    <span class="font-14 font-weight-bold">{{@$match['away_name']}}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-2 order-1 order-md-3">
                                    <div class="d-flex justify-content-end justify-content-md-center text-white">
                                        <a href="{{ route('live-football.show',['id'=>$match['id']]) }}"><i class="fas fa-play-circle text-white"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('script')
   {{-- <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>--}}
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
