<div class="container-fluid p-0 bg-black d-none d-lg-block">
    <div class="container py-md-3 py-0">
        <div class="row">
            <div class="col-md-4">
                <div id="logo" class="btn-over d-flex">
                    <a href="/"><img class="img-fluid" src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }}"></a>
                </div>
            </div>
            <div class="col-md-8">
                <div class="d-flex justify-content-end">
                    <ul class="d-block text-white font-14">
                        <i class="fas fa-phone-alt phone"></i>
                        <li class="d-inline"><a href="tel:060632888" class="text-white text-over">+855 60-63-2888</a> | </li>
                        <li class="d-inline"><a href="tel:061632888" class="text-white text-over">+855 61-63-2888</a> | </li>
                        <li class="d-inline"><a href="tel:069632888" class="text-white text-over">+855 69-63-2888</a></li>
                    </ul>
                </div>
                <div class="d-flex justify-content-end">
                    <ul class="social-network social-circle d-block m-0 mr-3">
                        <li><a href="https://www.facebook.com/AA2888Cambodia" target="_blank" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://t.me/aa2888online" target="_blank" class="icoTelegram" title="Telegram"><i class="fab fa-telegram-plane"></i></a></li>
                    </ul>
                    <ul class="d-block m-0">
                        <li class="d-inline-flex">
                            <div class="btn-over">
                                <a href="https://www.messenger.com/t/614516965282208" target="_blank" class="btn font-14 font-weight-bold">Register Now!</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--- Navigation --->
<nav id="navbar" class="navbar navbar-expand-lg navbar-dark bg-goldenrod sidebarNavigation" data-sidebarClass="navbar-dark">
    <div class="container-xl container-fluid">
        <a href="/" class="navbar-brand py-0 d-lg-none">
            <img src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image img-fluid">
        </a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <button type="button" class="close d-lg-none" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <a href="/" class="second-brand d-flex justify-content-center d-lg-none mb-5">
                <img src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image">
                {{--<span class="brand-text font-weight-light text-uppercase">Poraman</span>--}}
            </a>
            <ul class="nav navbar-nav nav-flex-icons m-auto">
                <li class="nav-item">
                    <a class="nav-link pl-0" href="/">
                        <i class="{{ config('global.icon_home') }}"></i>
                        Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('sport') }}">
                        <i class="fas fa-futbol"></i>
                        Sports
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('casino') }}">
                        <i class="bi bi-suit-spade-fill"></i>
                        Casino
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('live-casino') }}">
                        <i class="bi bi-play-fill"></i>
                        Live Casino
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('live-football') }}">
                        <i class="bi bi-broadcast-pin color-red"></i>
                        Live Football
                    </a>
                </li>
                {{--<li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="fas fa-question-circle"></i>
                        Help
                    </a>
                </li>--}}
            </ul><!-- .navbar-nav -->
            <div class="mt-3 d-lg-none d-block text-center">
                <ul class="social-network social-circle d-block m-0 mb-3">
                    <li class="border-0"><a href="https://www.facebook.com/AA2888Cambodia" target="_blank" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="border-0"><a href="https://t.me/aa2888online" target="_blank" class="icoTelegram" title="Telegram"><i class="fab fa-telegram-plane"></i></a></li>
                </ul>
                <ul class="d-block m-0">
                    <li class="d-inline-flex border-0">
                        <div class="btn-over">
                            <a href="#" class="btn font-14 font-weight-bold">Register Now!</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div><!-- #navbarsExampleDefault -->
    </div>
</nav>

<!-- search overlay -->
<div class="search-overlay">
    <button id="close-btn" type="button" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <form action="">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-sm-12 offset-sm-0">
                    <div class="input-group form-btn">
                        <input type="text" name="txt_search" class="form-control border-bottom" placeholder="Search here..." aria-describedby="button-submit">
                        <div class="input-group-append">
                            <button class="btn btn-outline-light brd-left-none border-bottom" type="button" id="button-submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
