<div id="footer" class="container-fluid pt-4 px-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-3">
                <div id="paymentMethod" class="text-center">
                    <h5 class="text-white text-uppercase mb-3">Payment Method</h5>
                    <div class="icon-container">
                        <i class="icon-img"><img src="{{ asset("img/cards/small-visa.png") }}" alt="Visa Card"></i>
                        <i class="icon-img"><img src="{{ asset("img/cards/small-master.png") }}" alt="Master Card"></i>
                       {{-- <i class="icon-img"><img src="{{ asset("img/cards/small-aba.jpg") }}" alt="ABA Bank"></i>
                        <i class="icon-img"><img src="{{ asset("img/cards/small-acleda.jpg") }}" alt="Acleda Bank"></i>--}}
                        <i class="icon-img"><img src="{{ asset("img/cards/small-wing.png") }}" alt="Wing"></i>
                        <i class="icon-img"><img src="{{ asset("img/cards/small-true.png") }}" alt="True Money"></i>
                        <i class="icon-img"><img src="{{ asset("img/cards/small-pipay.png") }}" alt="Pi Pay"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 mb-3">
                <h5 class="text-center text-white text-uppercase mb-3">Contact Us</h5>
                <ul class="d-block text-center text-white font-14 m-0">
                    <i class="fas fa-phone-alt phone"></i>
                    <li class="d-inline"><a href="tel:060632888" class="text-white text-over">+855 60-63-2888</a> | </li>
                    <li class="d-inline"><a href="tel:061632888" class="text-white text-over">+855 61-63-2888</a> | </li>
                    <li class="d-inline"><a href="tel:069632888" class="text-white text-over">+855 69-63-2888</a></li>
                    <li><i class="far fa-envelope"></i> <a href="mailto:aa2888cambodia@gmail.com" class="text-white text-over">aa2888cambodia@gmail.com</a> </li>
                </ul>
            </div>
            <div class="col-lg-3 mb-3">
                <h5 class="text-center text-white text-uppercase mb-3">Social Media</h5>
                <div class="text-center">
                    <ul class="social-network social-circle m-0">
                        <li><a href="https://www.facebook.com/AA2888Cambodia" target="_blank" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://t.me/aa2888online" target="_blank" class="icoTelegram" title="Telegram"><i class="fab fa-telegram-plane"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCo4QhOtd4h5yy3UxSwkUXlw" target="_blank" class="icoYoutube" title="Youtube"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copy mt-lg-3 py-2">
        <p class="text-white text-center text-capitalize mb-0">&copy; Copyright {{ $host_name }} 2021, All Rights reserve. | 18+</p>
    </div>
</div><!-- Footer -->
