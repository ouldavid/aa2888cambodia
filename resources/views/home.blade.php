@extends('layouts.master')

@section('title', $host_name.' | We offer unlimited services, easy to withdraw / deposit 24 hours, The most reliable in Cambodia.')
@section('title-social', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

<!-- include banner -->
@section('banner')
    <div id="banner-slide">
        <div class="slide">
            <div class="bg-slide" style="background-image: url({{ asset('img/banner/slide-01.jpg?v='.$version) }})"></div>
            <img class="img-fluid img-slide" src="{{ asset('img/banner/slide-01-mobile.jpg?v='.$version) }}" alt="{{ $host_name }}" />
        </div>
        <div class="slide">
            <div class="bg-slide" style="background-image: url({{ asset('img/banner/slide-02.jpg?v='.$version) }})"></div>
            <img class="img-fluid img-slide" src="{{ asset('img/banner/slide-02-mobile.jpg?v='.$version) }}" alt="{{ $host_name }}" />
        </div>
    </div>
@endsection

@section('content')
    <div class="position-relative">
        <div id="particles-js"></div>
        <div class="container py-5">
            <div id="liveFootball" class="position-relative text-center mb-5">
                <div class="d-flex justify-content-center mb-3">
                    <a href="#" class="color-gold text-uppercase text-over a-over"><h5>Live Football</h5></a>
                </div>
                <div id="football-slide-wrap">
                    <div id="football-slide">
                        @foreach(@$items as $match)
                            <div class="slide">
                                <a href="{{ route('live-football.show',['id'=>@$match['id']]) }}">
                                    <ul class="row d-flex align-items-center">
                                        <li class="col-lg-4 col-12 d-flex justify-content-lg-end justify-content-center">
                                            <img class="img-fluid" src="{{ asset(@$match['home_logo']) }}" alt="{{@$match['home_name']}}" />
                                        </li>
                                        <li class="col-lg-4 col-12 text-center">
                                            <span>{{@$match['match_date']}}</span>
                                            <small class="mb-2">{{@$match['match_time']}}</small>
                                            @if(\Carbon\Carbon::now()->between(
                                                    @$match['match_date_time'],
                                                    \Carbon\Carbon::parse(@$match['match_date_time'])->addMinutes(95)->format('Y-m-d H:i:s'), true))
                                                <span><img class="img-fluid m-auto w-50 h-auto" src="{{ asset("img/footballs/live.gif") }}" alt="AA2888 Cambodia" /></span>
                                            @endif
                                        </li>
                                        <li class="col-lg-4 col-12 d-flex justify-content-lg-start justify-content-center">
                                            <img class="img-fluid" src="{{ asset(@$match['away_logo']) }}" alt="{{@$match['away_name']}}" />
                                        </li>
                                    </ul>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div><!-- #liveFootball -->

            <div id="liveCasino" class="position-relative mb-3">
                <div class="d-flex justify-content-center mb-3">
                    <a href="#" class="color-gold text-uppercase text-over a-over"><h5>Live Casino</h5></a>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/live-casino/live-casino-01.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/live-casino/live-casino-01.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/live-casino/live-casino-01.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                </div>
            </div><!-- #liveCasino -->

            <div id="sportCasino" class="position-relative mb-3">
                <div class="d-flex justify-content-center mb-3">
                    <a href="#" class="color-gold text-uppercase text-over a-over"><h5>Sports and Casino Games</h5></a>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/sports/sport-01.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/games/game-01.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/games/game-02.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>

                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/games/game-03.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/games/game-03.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3">
                        <div class="img-over">
                            <a href="#"><img class="img-fluid" src="{{ asset("img/games/game-03.png") }}" alt="AA2888 Cambodia" /></a>
                        </div>
                    </div>
                </div>
            </div><!-- #sportCasino -->
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
